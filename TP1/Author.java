/**
 * @author eymondcy
 * @author castelan
 */

package tp1;

public class Author {
    private final String firstName;
    private final String lastName;
    
    /**
     * Constructeur
     * @param firstName Le prénom de l'auteur
     * @param lastName Le nom de l'auteur
     */
    public Author(String firstName, String lastName){
        this.firstName=firstName;
        this.lastName=lastName;
    }
    
    @Override
    /**
     * Methode qui permet de comparer l'objet passé en paramètre avec l'objet instancié
     * @param o Objet à comparer avec l'instance
     * @return true si l'objet passé en paramètre est égal à l'instance de la classe, false sinon
     */
    public boolean equals(Object o){
        if(!(o instanceof Author)){
            return false;
        }
        Author author=(Author)o;
        return(author.firstName.equals(this.firstName)&&(author.lastName.equals(this.lastName)));
    }
}